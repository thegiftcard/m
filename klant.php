<?php
require 'includes/db.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Magreth Koning - Paarden Massage</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<link rel="stylesheet" type="text/css" href="includes/klant_style.css" />
</head>

<body>

	<div id="page-wrap">
		<?php
		if (isset($_GET['hash']))
		{ //user logging in
		  $hash = filter_input(INPUT_GET, 'hash', FILTER_SANITIZE_STRING);
		  $id = filter_input(INPUT_GET, 'restinfo', FILTER_SANITIZE_STRING);
			if ($hash == "Rf7kBgIHw4Muunlh")
		  {
				//echo "Klant check: Hash is correct, laad de pagina<BR>";
				// Check of het ID al de pagina ingevuld hebt, anders toon je minimale pagina met een bedankje en dat Mar de info kan updaten!
				$result = mysqli_query($link_db_margreth,"SELECT * FROM `mar_klanten` where id = '$id'");
				while($row = mysqli_fetch_array($result))
				{
				  $uitst_verzoek = $row['uitst_verzoek'];
					$upd_mar_klant_email = $row['upd_mar_klant_email'];
				}
				if($uitst_verzoek != "N"){ echo "<p>Beste klant, <BR><BR>U heeft uw informatie al doorgeven aan Margreth Koning, daardoor deze pagina onbruikbaar geworden. <BR><BR> U contact contact opnemen met Margreth Koning via haar telefoon nummer.<BR><BR>Mocht u deze melding onterecht ontvangen, neem dan contact op met Margreth Koning, met de volgende fout code: 1x1337"; exit(); }
				//echo $uitst_verzoek."<BR>Laad pagina na de check!";
			} else {
				echo "<p>Er is een fout tijdens het laden van de pagina.<BR><BR> Blijft deze fout bestaan? Neem dan contact op met Margreth Koning. Geef de volgende fout code door: 0x1337";
				// DB beveiliging, HASH moet goed zijn, anders kunnen onbevoegde de pagina bekijken en DB queries doen.
				// echo "Beste klant, uw gegevens zijn momenteel niet bekend. Neem contact op met Margreth voor meer informatie!"
				 exit();
				//
			}
		}
		 ?>

		<p>Beste klant. (<?php echo "$upd_mar_klant_email"; ?>)</p>
		<p>U heeft een mail ontvangen van Margreth Koning, met het verzoek om informatie te verschaffen over u, uw paard en de stalling</p>
		<p>Zodra u onderstaand formulier ingevuld en verzonden heeft, zal Margreth op de hoogte gebracht worden. </p>
		<p>Voor eventuele vragen kunt u contact opnemen met Margreth Koning, via +31615441656 .</p>

		<div id="contact-area">

			<form  action="klant_form_verzend.php?call=send" method="post"><BR><BR>
				<HR>
					<p><H2>Gegevens Eigenaar</H2></p>
					<input type="hidden" value="<?php echo "$id"; ?>" name="form_klant_eig_id" id="form_klant_eig_id" />
					<input type="hidden" value="<?php echo "$upd_mar_klant_email"; ?>" name="form_klant_eig_mail" id="form_klant_eig_mail" />
				<label for="form_klant_eig_mail">Mail:</label>						<input type="text" name="form_klant_eig_mail" id="form_klant_eig_mail"value= "<?php echo "$upd_mar_klant_email"; ?>" disabled/>
				<label for="form_klant_eig_naam">Naam:</label>						<input type="text" name="form_klant_eig_naam" id="form_klant_eig_naam" />
				<label for="form_klant_eig_adres">Adres:</label>					<input type="text" name="form_klant_eig_adres" id="form_klant_eig_adres" />
				<label for="form_klant_eig_postc">postcode:</label>				<input type="text" name="form_klant_eig_postc" id="form_klant_eig_postc" />
				<label for="form_klant_eig_woonp">Woonplaats:</label>			<input type="text" name="form_klant_eig_woonp" id="form_klant_eig_woonp" />
				<label for="form_klant_eig_telnr">Telefoon nr:</label>		<input type="text" name="form_klant_eig_telnr" id="form_klant_eig_telnr" />
				<BR><BR><BR><BR>
					<hr>
				<p><H2>Gegevens stal</H2></p>

				<label for="form_klant_stl_naam">Naam:</label>				<input type="text" name="form_klant_stl_naam" id="form_klant_stl_naam" />
				<label for="form_klant_stl_adres">Adres:</label>			<input type="text" name="form_klant_stl_adres" id="form_klant_stl_adres" />
				<label for="form_klant_stl_plaat">Plaats:</label>			<input type="text" name="form_klant_stl_plaat" id="form_klant_stl_plaat" />
			<BR><BR><BR><BR>
				<hr>
			<p><H2>Gegevens Paard</H2></p>

			<label for="form_klant_prd_naam">Naam:</label>								<input type="text" name="form_klant_prd_naam" id="form_klant_prd_naam" />
			<label for="form_klant_prd_ras">Ras:</label>									<input type="text" name="form_klant_prd_ras" id="form_klant_prd_ras" />
			<label for="form_klant_prd_afst">Afstamming:</label>					<input type="text" name="form_klant_prd_afst" id="form_klant_prd_afst" />
			<label for="form_klant_prd_leef">Leeftijd:</label>						<input type="text" name="form_klant_prd_leef" id="form_klant_prd_leef" />
			<label for="form_klant_prd_gesl">Geslacht:</label>						<input type="text" name="form_klant_prd_gesl" id="form_klant_prd_gesl" />
			<label for="form_klant_prd_arts">Dierenarts:</label>					<input type="text" name="form_klant_prd_arts" id="form_klant_prd_arts" />
			<label for="form_klant_prd_smid">Hoefsmid:</label>						<input type="text" name="form_klant_prd_smid" id="form_klant_prd_smid" />
			<label for="form_klant_prd_zad1">Zadelmaker:</label>					<input type="text" name="form_klant_prd_zad1" id="form_klant_prd_zad1" />
			<label for="form_klant_prd_zad2">Datum zadelcontrole:</label>	<input type="text" name="form_klant_prd_zad2" id="form_klant_prd_zad2" />
		<label for="Message">Rede van contact:</label><br /><textarea name="Message" rows="20" cols="20" id="Message"></textarea>

				<input type="submit" name="submit" value="Submit" class="submit-button" />
			</form>

			<div style="clear: both;"></div>

			<p>Uw informatie is naar Margreth Koning verzonden, er wordt zsm contact met u opgenomen.</p>
		</div>

	</div>

</body>

</html>
