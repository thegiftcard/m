<?php
require 'db.php';



if (isset($_GET['call']))
{ //user logging in
  $my_string = filter_input(INPUT_GET, 'call', FILTER_SANITIZE_STRING);
  $my_string_coin = filter_input(INPUT_POST, 'searchterm', FILTER_SANITIZE_STRING);
  $mail_adres = filter_input(INPUT_POST, 'upd_mar_klant_email', FILTER_SANITIZE_STRING);
	$form_klant_eig_id = filter_input(INPUT_POST, 'upd_mar_klant_id', FILTER_SANITIZE_STRING);
	$form_klant_eig_mail = filter_input(INPUT_POST, 'upd_mar_klant_email', FILTER_SANITIZE_STRING);
	$form_klant_eig_naam = filter_input(INPUT_POST, 'upd_mar_klant_naam', FILTER_SANITIZE_STRING);
	$form_klant_eig_adres = filter_input(INPUT_POST, 'upd_mar_klant_adres', FILTER_SANITIZE_STRING);
	$form_klant_eig_postc = filter_input(INPUT_POST, 'upd_mar_klant_postcode', FILTER_SANITIZE_STRING);
	$form_klant_eig_woonp = filter_input(INPUT_POST, 'upd_mar_klant_woonplaats', FILTER_SANITIZE_STRING);
	$form_klant_eig_telnr = filter_input(INPUT_POST, 'upd_mar_klant_telefoon', FILTER_SANITIZE_STRING);
	$form_klant_stl_naam = filter_input(INPUT_POST, 'upd_mar_stal_naam', FILTER_SANITIZE_STRING);
	$form_klant_stl_adres = filter_input(INPUT_POST, 'upd_mar_stal_adres', FILTER_SANITIZE_STRING);
	$form_klant_stl_plaat = filter_input(INPUT_POST, 'upd_mar_stal_plaats', FILTER_SANITIZE_STRING);
	$form_klant_prd_naam = filter_input(INPUT_POST, 'upd_mar_paard_naam', FILTER_SANITIZE_STRING);
	$form_klant_prd_ras = filter_input(INPUT_POST, 'upd_mar_paard_ras', FILTER_SANITIZE_STRING);
	$form_klant_prd_afst = filter_input(INPUT_POST, 'upd_mar_paard_afstamming', FILTER_SANITIZE_STRING);
	$form_klant_prd_leef = filter_input(INPUT_POST, 'upd_mar_paard_leeftijd', FILTER_SANITIZE_STRING);
	$form_klant_prd_gesl = filter_input(INPUT_POST, 'upd_mar_paard_geslacht', FILTER_SANITIZE_STRING);
	$form_klant_prd_arts = filter_input(INPUT_POST, 'upd_mar_paard_dierenarts', FILTER_SANITIZE_STRING);
	$form_klant_prd_smid = filter_input(INPUT_POST, 'upd_mar_paard_hoefsmid', FILTER_SANITIZE_STRING);
	$form_klant_prd_zad1 = filter_input(INPUT_POST, 'upd_mar_paard_zadelmaker', FILTER_SANITIZE_STRING);
	$form_klant_prd_zad2 = filter_input(INPUT_POST, 'upd_mar_paard_zadelmaker_ctrl', FILTER_SANITIZE_STRING);
  $form_klant_txtarea_rede = filter_input(INPUT_POST, 'upd_mar_txtarea_rede', FILTER_SANITIZE_STRING);
  $form_klant_txtarea_anam = filter_input(INPUT_POST, 'upd_mar_txtarea_anam', FILTER_SANITIZE_STRING);
  $form_klant_txtarea_insp = filter_input(INPUT_POST, 'upd_mar_txtarea_insp', FILTER_SANITIZE_STRING);
  $form_klant_txtarea_palp_voor = filter_input(INPUT_POST, 'upd_mar_txtarea_palp_voor', FILTER_SANITIZE_STRING);
  $form_klant_txtarea_palp_bewegen = filter_input(INPUT_POST, 'upd_mar_txtarea_palp_bewegen', FILTER_SANITIZE_STRING);
  $form_klant_txtarea_massage = filter_input(INPUT_POST, 'upd_mar_txtarea_massage', FILTER_SANITIZE_STRING);
  $form_klant_txtarea_huiswerk = filter_input(INPUT_POST, 'upd_mar_txtarea_huiswerk', FILTER_SANITIZE_STRING);
	$Message = filter_input(INPUT_POST, 'Message', FILTER_SANITIZE_STRING);
	// TEMP:
	//$mail_adres = "e@e.c";
	$mail_adres = $form_klant_eig_mail;


  if (filter_var($mail_adres, FILTER_VALIDATE_EMAIL)) {
  //echo("$mail_adres is a valid email address");
  } else {
    echo("<BR><BR>$mail_adres is geen correct mail adres, mail kan niet verzonden worden!!<BR>");
    exit;
  }

  if ($my_string == "TEMPLATE")
  {
    //
  }

  if ($my_string == "send")
  {

//		echo("info verzonden!<BR>");
//		foreach($_POST as $key => $value) {
//       echo "<B>Key:</B> ".$key." <B>Data:</B> ".$value."<BR>";

	//$sql = "update into mar_klanten(id,uitst_verzoek,upd_mar_klant_email,reg_date) values('','N','$mail_adres', CURRENT_TIMESTAMP)";
//}

	$sql = "UPDATE mar_klanten SET uitst_verzoek='Y', upd_mar_klant_email='$form_klant_eig_mail', upd_mar_klant_naam='$form_klant_eig_naam' ,upd_mar_klant_adres='$form_klant_eig_adres' ,upd_mar_klant_postcode='$form_klant_eig_postc' ,upd_mar_klant_woonplaats='$form_klant_eig_woonp' ,upd_mar_klant_telefoon='$form_klant_eig_telnr' ,upd_mar_stal_naam='$form_klant_stl_naam' ,upd_mar_stal_adres='$form_klant_stl_adres' ,upd_mar_stal_plaats='$form_klant_stl_plaat' ,upd_mar_paard_naam='$form_klant_prd_naam' ,upd_mar_paard_ras='$form_klant_prd_ras' ,upd_mar_paard_afstamming='$form_klant_prd_afst' ,upd_mar_paard_leeftijd='$form_klant_prd_leef' ,upd_mar_paard_geslacht='$form_klant_prd_gesl' ,upd_mar_paard_dierenarts='$form_klant_prd_arts' ,upd_mar_paard_hoefsmid='$form_klant_prd_smid' ,upd_mar_paard_zadelmaker='$form_klant_prd_zad1' ,upd_mar_paard_zadelmaker_ctrl='$form_klant_prd_zad2' ,upd_mar_txtarea_rede='$form_klant_txtarea_rede' ,upd_mar_txtarea_anam='$form_klant_txtarea_anam' ,upd_mar_txtarea_insp='$form_klant_txtarea_insp' ,upd_mar_txtarea_palp_voor='$form_klant_txtarea_palp_voor' ,upd_mar_txtarea_palp_bewegen='$form_klant_txtarea_palp_bewegen' ,upd_mar_txtarea_massage='$form_klant_txtarea_massage' ,upd_mar_txtarea_huiswerk='$form_klant_txtarea_huiswerk' WHERE id = $form_klant_eig_id ";


//	echo $sql;
	mysqli_query($link_db_margreth,$sql);
  echo "Database is geupdate.";
	}
}

?>
