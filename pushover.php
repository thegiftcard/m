<?php
private static function pushNotification($userKey, $message, $title = null, $url = null, $urltitle = null)
 {
     Notifications::addLog('Pushover[pushNotification' . ']; $userKey=[' . $userKey . ']; $message=[' . $message . ']; $title=[' . $title . ']; $url=[' . $url . ']; $urltitle=[' . $urltitle . ']', 'MESSAGING');
     $notification = new Pushover();
     $token = Config::get('applicationToken', 'msg_pushover');
     if (is_null($token)) {
         throw new Exception("Pushover - Application token not specified", 500);
     }
     if (is_null($userKey)) {
         throw new Exception("Pushover - User key not specified", 500);
     }
     $notification->setToken($token);
     $notification->setUser($userKey);
     $notification->setMessage($message);
     if (!is_null($title)) {
         $notification->setTitle($title);
     }
     $notification->setHtml(1);
     $notification->setUrl($url);
     $notification->setUrlTitle($urltitle);
     if (!$notification->send()) {
         Notifications::addError("Pushover - Error in sending a notification to '{$userKey}'");
     } else {
         Notifications::addSuccess('Pushover message sent.');
     }
 }
 ?>
